
let recipe = {
  'title': 'Adobo',
  'servings': 5,
  'ingredients': ['chicken', 'soy sauce', 'onions']
};

console.log(recipe.title);
console.log('Servings: ' + recipe.servings);
console.log('Ingredients:');

let i;

for (i = 0; i < recipe.ingredients.length; i++) {
  console.log(recipe.ingredients[i]);
}


let books = [
{
   title: "A GAME OF THRONES (A SONG OF ICE AND FIRE, BOOK ONE)",
   author: 'George R.R. Martin',
   alreadyRead: false
},
{
   title: "A CLASH OF KINGS (A SONG OF ICE AND FIRE, BOOK TWO)",
   author: 'Brian Christian',
   alreadyRead: true
}];

let book;
let bookInfo;

for (i = 0; i < books.length; i++) {
 book = books[i];
 bookInfo = book.title + '" by ' + book.author;
 if (book.alreadyRead) {
  console.log('You already read "' + bookInfo);
} else {
  console.log('You still need to read "' + bookInfo);
}
}