console.log("Hello World");

function FinalFantasy(name,job,level,status){
	//Properties
	this.name = name;
	this.job = job;
	this.level = level;
	this.hp = 2*level;
	this.attack = level;
	this.status = status;

}

let squall = new FinalFantasy("Squall", "Swordsman",999,9999);
let cloud = new FinalFantasy("Cloud", "Assassin",109, 9999);
let tidus = new FinalFantasy("Tidus", "Hunter",99, 9999);

console.log(squall);
console.log(cloud);
console.log(tidus);