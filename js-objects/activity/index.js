console.log("Hello World");


let books = {
	book1: { 
	name: 'George R.R. Martin',
	title:  "A GAME OF THRONES A SONG OF ICE AND FIRE, BOOK ONE",
	datePublished: "August 1, 1996",
	summary: "Long ago, in a time forgotten, a preternatural event threw the seasons out of balance. In a land where summers can last decades and winters a lifetime, trouble is brewing. The cold is returning, and in the frozen wastes to the north of Winterfell, sinister and supernatural forces are massing beyond the kingdom’s protective Wall.",

	},
	book2: { 
	name: 'George R.R. Martin',
	title:  "A CLASH OF KINGS A SONG OF ICE AND FIRE, BOOK TWO",
	datePublished: "November 16, 1998",
	summary: "A comet the color of blood and flame cuts across the sky. And from the ancient citadel of Dragonstone to the forbidding shores of Winterfell, chaos reigns. Six factions struggle for control of a divided land and the Iron Throne of the Seven Kingdoms, preparing to stake their claims through tempest, turmoil, and war.",

	},
	book3: { 
	name: 'George R.R. Martin',
	title:  "A STORM OF SWORDS A SONG OF ICE AND FIRE, BOOK THREE",
	datePublished: "May 28, 2002",
	summary: "Of the five contenders for power, one is dead, another in disfavor, and still the wars rage as violently as ever, as alliances are made and broken. Joffrey, of House Lannister, sits on the Iron Throne, the uneasy ruler of the land of the Seven Kingdoms. His most bitter rival, Lord Stannis, stands defeated and disgraced, the victim of the jealous sorceress who holds him in her evil thrall. But young Robb, of House Stark, still rules the North from the fortress of Riverrun.",

	}

}
console.log(books.book1);
console.log(books.book2);
console.log(books.book3);

